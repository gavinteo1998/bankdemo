package com.example.bankdemo;

public class BankModelClass {

    String accountBalance;
    String accountLabel;
    String accountAvailable;
    String accountCurrent;
    String accountTransactions;
    String accountAmount;
    String accountDescription;

    public BankModelClass(String accountBalance, String accountLabel,String accountAvailable,String accountCurrent, String accountTransactions,
                          String accountAmount, String accountDescription){
        this.accountBalance = accountBalance;
        this.accountLabel = accountLabel;
        this.accountAvailable = accountAvailable;
        this.accountCurrent = accountCurrent;
        this.accountTransactions = accountTransactions;
        this.accountDescription = accountDescription;
        this.accountAmount = accountAmount;
    }

    public BankModelClass(){

    }

    public String getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(String accountBalance) {
        this.accountBalance = accountBalance;
    }

    public String getAccountLabel() {
        return accountLabel;
    }

    public void setAccountLabel(String accountLabel) {
        this.accountLabel = accountLabel;
    }

    public String getAccountAvailable() {
        return accountAvailable;
    }

    public void setAccountAvailable(String accountAvailable) {
        this.accountAvailable = accountAvailable;
    }

    public String getAccountCurrent() {
        return accountCurrent;
    }

    public void setAccountCurrent(String accountCurrent) {
        this.accountCurrent = accountCurrent;
    }

    public String getAccountTransactions() {
        return accountTransactions;
    }

    public void setAccountTransactions(String accountTransactions) {
        this.accountTransactions = accountTransactions;
    }

    public String getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(String accountAmount) {
        this.accountAmount = accountAmount;
    }

    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }
}
