package com.example.bankdemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.MyViewHolder> {

    private Context mContext;
    //store bank data
    private List<BankModelClass> bankData;

    // pass in the bankdata array into constructor and context
    public BankAdapter(Context mContext, List<BankModelClass> bankData){
        this.mContext = mContext;
        this.bankData = bankData;
    }


    @NonNull
    @Override
    public BankAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        // create instance of layout
        LayoutInflater inflater = LayoutInflater.from(mContext);
        v = inflater.inflate(R.layout.bank_item, parent, false);

        //return new holder instance
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        // set items views based on views and data model from bankmodelclass
        holder.accountBalance.setText(bankData.get(position).getAccountBalance());
        holder.accountLabel.setText(bankData.get(position).getAccountLabel());
        holder.accountAvailable.setText(bankData.get(position).getAccountAvailable());
        holder.accountCurrent.setText(bankData.get(position).getAccountCurrent());
        holder.accountTransactions.setText(bankData.get(position).getAccountTransactions());
        holder.accountNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // upon clicking the button , passed data between activities by using intent
                Intent intent = new Intent(mContext,MainActivity2.class);
                intent.putExtra("transactions", bankData.get(position).getAccountTransactions());
                intent.putExtra("accountLabel", bankData.get(position).getAccountLabel());
                intent.putExtra("accountBalance", bankData.get(position).getAccountBalance());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        // return total count of items in the list
        return bankData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // holder that contains variable
        // for any view that will be set as rendering as a row
        TextView accountBalance;
        TextView accountLabel;
        TextView accountAvailable;
        TextView accountCurrent;
        TextView accountTransactions;
        Button accountNext;

        public MyViewHolder(@NonNull View itemView){
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            accountBalance = itemView.findViewById(R.id.amount);
            accountLabel = itemView.findViewById(R.id.description);
            accountCurrent = itemView.findViewById(R.id.accountCurrent);
            accountAvailable = itemView.findViewById(R.id.accountAvailable);
            accountNext = itemView.findViewById(R.id.button);
            accountTransactions = itemView.findViewById(R.id.accountTransaction);


        }
    }
}
