package com.example.bankdemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {

    private Context mContext;
    private List<BankModelClass> bankData;


    public TransactionAdapter(Context mContext, List<BankModelClass> bankData){
        this.mContext = mContext;
        this.bankData = bankData;
    }


    @NonNull
    @Override
    public TransactionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        v = inflater.inflate(R.layout.transaction_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        // get data from Bank Model Class from getter function
        holder.accountDescription .setText(bankData.get(position).getAccountDescription());
        holder.accountAmount.setText(bankData.get(position).getAccountAmount());

    }

    @Override
    public int getItemCount() {
        return bankData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView accountDescription;
        TextView accountAmount;


        public MyViewHolder(@NonNull View itemView){
            super(itemView);

            accountDescription = itemView.findViewById(R.id.amount);
            accountAmount = itemView.findViewById(R.id.description);

        }
    }
}
